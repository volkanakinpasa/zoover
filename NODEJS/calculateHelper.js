var moment = require('moment');
var _ = require('lodash-node');
var now = new Date();

var calculateHelper = function() {

    this.calculateGeneralRating = function(reviews) {

        //Here is a normal avarage. it is same as any of any hotels' rate on the web site.
        // var sum = _.sum(reviews, (r)=> r.ratings.general.general);
        // return (sum / reviews.length);

        //Here is the avarage as required in the question.
        var ratingSum = 0;

        _.forEach(reviews, function(review) {

            var currentRating = review.ratings.general.general;

            ratingSum += getWeight(currentRating, review.entryDate);

        });

        return (ratingSum / reviews.length);
    }

    this.calculateAspectsRating = function(reviews) {

        var aspectKeyRates = [];
        _.forEach(reviews, function(review) {
            _.forEach(review.ratings.aspects, function(value, key) {
                if(!_.some(aspectKeyRates, {'key': key})) {
                    if (value !=null && value > 0) {
                        var newItem = {};
                        newItem.key = key;
                        newItem.value = getWeight(value, review.entryDate);
                        newItem.count = 1;
                        aspectKeyRates.push(newItem);
                    }
                    
                }
                else {
                    if (value !=null && value > 0) {
                        var found = _.find(aspectKeyRates, {'key': key});
                        found.value += getWeight(value, review.entryDate);
                        found.count++;
                    }
                }
                
            });
        });

        _.forEach(aspectKeyRates, function(item) {

            item.rate = item.value / item.count;
            
        });
   
        return aspectKeyRates;
      
    }

    getWeight = function(value, entryDateParam) {

        var entryDate = new Date(entryDateParam);

        if (moment.duration(now - entryDate).years() > 5) {
            //I am not sure if I am doing this correctly! Because the question says "When the review is older than 5 years its weight value defaults to 0.5.". What does that mean?
            value = value * 0.5;   
        } 
        else {
            value = value * (1 - ((now.getUTCFullYear() - entryDate.getUTCFullYear()) * 0.1));
        }

        return value;
    }

}

exports.calculateHelper = calculateHelper;