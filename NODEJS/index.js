var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var Client = require('node-rest-client').Client;
var helper = require('./calculateHelper');
var calculateHelper = helper.calculateHelper();

var apiRootUrl = "http://localhost:50829/accommodations/";

app.get('/accommodations/:accommodationId', function (req, res) {
    var client = new Client(); 
    client.get(apiRootUrl + req.params.accommodationId + "/reviews", function (data, response) {
    
        var resultOfGeneral = helper.calculateGeneralRating(data);

        console.log(resultOfGeneral);

        var resultOfAspects = helper.calculateAspectsRating(data);

        res.setHeader('Content-Type', 'application/json');

        res.send(JSON.stringify({'general': resultOfGeneral, 'aspects': resultOfAspects}));
    }); 
});


app.listen(port);

console.log('listening on: ' + port);