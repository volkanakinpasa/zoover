﻿using Models;
using Newtonsoft.Json;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace API.Controllers
{
    public class AccommodationsController : ApiController
    {
        private readonly DBRepository repository;

        public AccommodationsController()
        {
            repository = new DBRepository(System.Configuration.ConfigurationManager.ConnectionStrings["default"].ConnectionString);
        }

        [Route("accommodations")]
        public IEnumerable<Accommodation> Get()
        {
            return JsonConvert.DeserializeObject<List<Accommodation>>(repository.Read("accommodations"));
        }

        [Route("accommodations/{id}")]
        public IHttpActionResult Get(Guid id)
        {
            List<Accommodation> accommodations = JsonConvert.DeserializeObject<List<Accommodation>>(repository.Read("accommodations"), new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });

            var accommodation = accommodations.FirstOrDefault(a => a.id == id);

            if (accommodation == null)
            {
                return NotFound();
            }

            return Ok(accommodation);
        }
    }
}
