﻿using Models;
using Newtonsoft.Json;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace API.Controllers
{
    public class ReviewsController : ApiController
    {
        private readonly DBRepository repository;
        public ReviewsController()
        {
            repository = new DBRepository(System.Configuration.ConfigurationManager.ConnectionStrings["default"].ConnectionString);
        }

        [Route("accommodations/{accommodationId}/reviews")]
        public IHttpActionResult Get(Guid accommodationId)
        {
            List<Review> reviews = JsonConvert.DeserializeObject<List<Review>>(repository.Read("reviews"), new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });

            reviews = reviews.Where(r => r.parents.Any(rp => rp.id == accommodationId)).ToList();

            if (!reviews.Any())
            {
                return NotFound();
            }

            return Ok(reviews);
        }

        [Route("accommodations/{accommodationId}/reviews/{reviewId}")]
        public IHttpActionResult Get(Guid accommodationId, Guid reviewId)
        {
            List<Review> reviews = JsonConvert.DeserializeObject<List<Review>>(repository.Read("reviews"), new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });

            var review = reviews.Where(r => r.parents.Any(rp => rp.id == accommodationId) && reviewId == r.id).FirstOrDefault();

            if (review == null)
            {
                return NotFound();
            }

            return Ok(review);
        }
    }
}
