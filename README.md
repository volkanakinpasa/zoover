
# Zoover #

### How to bulk insert ###

 1. Run the script "Sql\Init.sql". It will create a database and tables.
 2. I created an executable application (.Net, C#) which reads the json files and inserts them into the tables.  to restore nuget packages and build, run the following script on powershell.
```powershell
./build.ps1
```
3. Run \IMPORTTOOL\bin\Release\Consoleapp1.exe. It will insert into the tables.
4. Host \API project under IIS or IIS express.

### How to run Node Js ###

 1. Update the api url in \Nodejs\index.js file.
 2. npm install
 3. node index.js
 4. open a browser with the url http://localhost:3000/accommodations/8a0d839a-e74e-4c0d-abaa-ce58b6b29ed5
