﻿using Repository;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var rootPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var accommodationsFullFilePath = Path.Combine(rootPath, "accommodations.json");
            var reviewsFullFilePath = Path.Combine(rootPath, "reviews.json");

            DBRepository accommodationRepository = new DBRepository(System.Configuration.ConfigurationManager.ConnectionStrings["default"].ConnectionString);

            ReadUtil readAndWrite = new ReadUtil();
            string accommodations = readAndWrite.Read(accommodationsFullFilePath);
            string reviews = readAndWrite.Read(reviewsFullFilePath);

            accommodationRepository.InsertJson("accommodations", accommodations);
            accommodationRepository.InsertJson("reviews", reviews);
        }
    }
}
