﻿namespace ConsoleApp1
{
    public class ReadUtil
    {
        public string Read(string fullFilePath)
        {
            return System.IO.File.ReadAllText(fullFilePath);
        }
    }
}
