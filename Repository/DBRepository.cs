﻿using System.Data.SqlClient;

namespace Repository
{
    public class DBRepository
    {
        private readonly string ConnectionString;

        public DBRepository(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public void InsertJson(string tableName, string json)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(string.Format("delete from {0}; insert into {0}(json) values(@json)", tableName), connection))
                {
                    command.Parameters.Add(new SqlParameter("@json", json));
                    command.ExecuteNonQuery();
                }
            }
        }

        public string Read(string tableName)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(string.Format("select json from {0}", tableName), connection))
                {
                    return (string)command.ExecuteScalar();
                }
            }
        }
    }
}
