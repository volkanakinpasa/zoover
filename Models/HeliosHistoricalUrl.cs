﻿namespace Models
{
    public class HeliosHistoricalUrl
    {
        public string oldUrl { get; set; }
        public string newUrl { get; set; }
    }
}
