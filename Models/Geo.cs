﻿using System.Collections.Generic;

namespace Models
{
    public class Geo
    {
        public IList<double> coordinates { get; set; }
        public string type { get; set; }
    }
}
