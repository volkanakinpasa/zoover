﻿using System;
using System.Collections.Generic;

namespace Models
{
    public class Accommodation
    {
        public int vrwId { get; set; }
        public Names names { get; set; }
        public string type { get; set; }
        public Media media { get; set; }
        public Geo geo { get; set; }
        public int stars { get; set; }
        public Address address { get; set; }
        public ContactInformation contactInformation { get; set; }
        public Awards awards { get; set; }
        public IList<Facility> facilities { get; set; }
        public string heliosUrl { get; set; }
        public IList<HeliosHistoricalUrl> heliosHistoricalUrls { get; set; }
        public bool bookingComBlockEnabled { get; set; }
        public bool premiumAdlinkEnabled { get; set; }
        public Owner owner { get; set; }
        public Status status { get; set; }
        public IList<Parent> parents { get; set; }
        public Guid id { get; set; }
        public int heliosId { get; set; }
        public object UpdatedDate { get; set; }
        public double popularityScore { get; set; }
        public string giataId { get; set; }
    }
}
