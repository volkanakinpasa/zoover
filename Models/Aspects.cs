﻿namespace Models
{
    public class Aspects
    {
        public int location { get; set; }
        public int service { get; set; }
        public int priceQuality { get; set; }
        public int food { get; set; }
        public int room { get; set; }
        public int childFriendly { get; set; }
        public object interior { get; set; }
        public object size { get; set; }
        public object activities { get; set; }
        public object restaurants { get; set; }
        public object sanitaryState { get; set; }
        public object accessibility { get; set; }
        public object nightlife { get; set; }
        public object culture { get; set; }
        public object surrounding { get; set; }
        public object atmosphere { get; set; }
        public object noviceSkiArea { get; set; }
        public object advancedSkiArea { get; set; }
        public object apresSki { get; set; }
        public object beach { get; set; }
        public object entertainment { get; set; }
        public object environmental { get; set; }
        public object pool { get; set; }
        public object terrace { get; set; }
        public object housing { get; set; }
        public object hygiene { get; set; }
    }

    //public class Status
    //{
    //    public bool published { get; set; }
    //    public bool @checked { get; set; }
    //    public string reason { get; set; }
    //}

    //public class Parent
    //{
    //    public string id { get; set; }
    //}

}
