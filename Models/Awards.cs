﻿using System.Collections.Generic;

namespace Models
{
    public class Awards
    {
        public IList<string> winnerYears { get; set; }
        public IList<string> recommendedYears { get; set; }
        public IList<string> highlyRecommendedYears { get; set; }
    }
}
