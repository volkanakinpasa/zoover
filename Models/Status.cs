﻿namespace Models
{
    public class Status
    {
        public bool published { get; set; }
        public bool @checked { get; set; }
        public string reason { get; set; }
    }
}
