﻿namespace Models
{
    public class ContactInformation
    {
        public string phone { get; set; }
        public string email { get; set; }
        public string url { get; set; }
    }
}
