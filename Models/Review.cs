﻿using System;
using System.Collections.Generic;

namespace Models
{
    public class Review
    {
        public User user { get; set; }
        public Titles titles { get; set; }
        public Texts texts { get; set; }
        public long travelDate { get; set; }
        public string locale { get; set; }
        public Ratings ratings { get; set; }
        public string traveledWith { get; set; }
        public Cooperation cooperation { get; set; }
        public long entryDate { get; set; }
        public string originalUserName { get; set; }
        public string originalUserEmail { get; set; }
        public string originalUserIp { get; set; }
        public Status status { get; set; }
        public IList<Parent> parents { get; set; }
        public Guid id { get; set; }
        public int heliosId { get; set; }
        public long updatedDate { get; set; }
    }

}
