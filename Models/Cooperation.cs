﻿namespace Models
{
    public class Cooperation
    {
        public string importPartner { get; set; }
        public SyndicationPartner syndicationPartner { get; set; }
    }

    //public class Status
    //{
    //    public bool published { get; set; }
    //    public bool @checked { get; set; }
    //    public string reason { get; set; }
    //}

    //public class Parent
    //{
    //    public string id { get; set; }
    //}

}
