﻿#tool "nuget:?package=NUnit.ConsoleRunner"

var target = Argument("target", "Start");
var configuration = Argument("configuration", "Release");
var solutionPathImport = File("./IMPORTTOOL.sln");
var solutionPathApi = File("./API.sln");

///////////////////////////////////////////////////////////////////////////////
// TASKS
///////////////////////////////////////////////////////////////////////////////

Task("Clean")
    .Does(() =>
{
	Information("Cleaning...");
    CleanDirectories("./**/bin/" + configuration);
});

Task("Restore-NuGet")
	.IsDependentOn("Clean")
    .Does(() =>
{
	Information("Restoring...");
    NuGetRestore(solutionPathImport);
	NuGetRestore(solutionPathApi);
});

Task("Build")
    .Does(() =>
{
	Information("Building...");
	 MSBuild(solutionPathImport, settings =>
        settings.SetPlatformTarget(PlatformTarget.MSIL)
            .SetMSBuildPlatform(MSBuildPlatform.x86)
            .SetVerbosity(Verbosity.Quiet)
            .WithTarget("Build")
            .SetConfiguration(configuration));
	MSBuild(solutionPathApi, settings =>
        settings.SetPlatformTarget(PlatformTarget.MSIL)
            .SetMSBuildPlatform(MSBuildPlatform.x86)
            .SetVerbosity(Verbosity.Quiet)
            .WithTarget("Build")
            .SetConfiguration(configuration));		
});

Task("Start")
 .IsDependentOn("Restore-NuGet")
  .IsDependentOn("Build")
.Does(() =>
{
	 
});

RunTarget(target);